const responseMiddleware = (req, res, next) => {
    // console.log(`RESPONSE MIDDLEWARE`);
    // console.log(`res.err: ${res.err}`);
    // console.log(`res.data: ${res.data}`);
    if(res.err) {
        res.status(400).send({
            error: true,
            message: res.err.toString()
        });
    } else if(!res.data) {
        res.status(404).send({
            error: true,
            message: 'Not Found'
        });
    } else if(res.data) {
        res.status(200).send(res.data);
    }
    next();
}

exports.responseMiddleware = responseMiddleware;