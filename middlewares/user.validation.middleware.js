const { user } = require('../models/user');
const { errorResponse, resp400 } = require('../helpers/responseHelper');
const createUserValid = (req, res, next) => {
    const userToInsert = req.body;

    // EMPTY
    if (!userToInsert) {
        res.status(400).send(errorResponse(resp400));
        // console.log('CREATE EMPTY');
        return;
    }

    // NUMBER_OF_FIELDS
    if (Object.keys(user).length - 1 !== Object.keys(userToInsert).length) {
        res.status(400).send(errorResponse(resp400));
        // console.log('CREATE NUMBER_OF_FIELDS');
        return;
    }

    // ID_SHOULD_BE_ABSENT
    if (userToInsert.id) {
        res.status(400).send(errorResponse(resp400));
        // console.log('CREATE ID_SHOULD_BE_ABSENT');
        return;
    }

    const notValid = Object.keys(user).find(key => {
        return userToInsert[key] === undefined && key !== 'id';
    });

    // ALL_FIELDS_MATCH
    if (notValid) {
        res.status(400).send(errorResponse(resp400));
        // console.log('CREATE ALL_FIELDS_MATCH', notValid);
        return;
    }

    // PHONE_REGEXP
    if (!validatePhone(userToInsert.phoneNumber)) {
        res.status(400).send(errorResponse(resp400));
        // console.log('CREATE PHONE_REGEXP');
        return;
    }

    // EMAIL_REGEXP
    if (!validateEmail(userToInsert.email)) {
        res.status(400).send(errorResponse(resp400));
        // console.log('CREATE EMAIL_REGEXP');
        return;
    }

    // PASSWORD_SHORT
    if(userToInsert.password.length < 3) {
        res.status(400).send(errorResponse(resp400));
        // console.log('CREATE PASSWORD_SHORT');
        return;
    }

    next();
}

const updateUserValid = (req, res, next) => {
    const userToInsert = req.body;

    // EMPTY
    if (!userToInsert) {
        res.status(400).send(errorResponse(resp400));
        // console.log('UPDATE EMPTY');
        return;
    }
    
    // ID_SHOULD_BE_ABSENT
    if (userToInsert.id) {
        res.status(400).send(errorResponse(resp400));
        // console.log('UPDATE ID_SHOULD_BE_ABSENT');
        return;
    }

    // PHONE_REGEXP
    if (userToInsert.phoneNumber && !validatePhone(userToInsert.phoneNumber)) {
        res.status(400).send(errorResponse(resp400));
        // console.log('UPDATE PHONE_REGEXP');
        return;
    }

    // EMAIL_REGEXP
    if (userToInsert.phoneNumber && !validateEmail(userToInsert.email)) {
        res.status(400).send(errorResponse(resp400));
        // console.log('UPDATE EMAIL_REGEXP');
        return;
    }

    // PASSWORD_SHORT
    if(userToInsert && userToInsert.password && userToInsert.password.length < 3) {
        res.status(400).send(errorResponse(resp400));
        // console.log('CREATE PASSWORD_SHORT');
        return;
    }

    notValid = Object.keys(userToInsert).find(key => {
        return user[key] === undefined && key !== 'id';
    });

    // ALL_FIELDS_MATCH    
    if (notValid) {
        res.status(400).send(errorResponse(resp400));
        // console.log('UPDATE ALL_FIELDS_MATCH');
        return;
    }

    next();
}

const validatePhone = (phone) => {
    const phoneRegexp = /^((0|\+380)-?)\d{2}-?\d{3}-?\d{2}-?\d{2}$/;
    return phoneRegexp.test(phone);
}

const validateEmail = (email) => {
    const gmailEmailRegexp = /^[a-zA-Z0-9_.+-]+@(?:(?:[a-zA-Z0-9-]+\.)?[a-zA-Z]+\.)?(gmail)\.com$/;
    return gmailEmailRegexp.test(email);
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;