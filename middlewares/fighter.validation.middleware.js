const { fighter } = require('../models/fighter');
const { errorResponse, resp400 } = require('../helpers/responseHelper');

const createFighterValid = (req, res, next) => {
    const fighterToInsert = req.body;

    // EMPTY
    if (!fighterToInsert) {
        res.status(400).send(errorResponse(resp400));
        // console.log('CREATE EMPTY');
        return;
    }

    // NUMBER_OF_FIELDS
    // Health is optional
    if (Object.keys(fighter).length - 2 !== Object.keys(fighterToInsert).length) {
        res.status(400).send(errorResponse(resp400));
        return;
    }

    // ID_SHOULD_BE_ABSENT
    if (fighterToInsert.id) {
        res.status(400).send(errorResponse(resp400));
        // console.log('CREATE ID_SHOULD_BE_ABSENT');
        return;
    }

    const notValid = Object.keys(fighter).find(key => {
        return fighterToInsert[key] === undefined && key !== 'id' && key !== 'health';
    });

    // ALL_FIELDS_MATCH
    if (notValid) {
        res.status(400).send(errorResponse(resp400));
        // console.log('CREATE ALL_FIELDS_MATCH', notValid);
        return;
    }

    // POWER_SHOULD_BE_NUMBER
    if(typeof fighterToInsert.power !== 'number') {
        // console.log('CREATE POWER_SHOULD_BE_NUMBER', notValid);
        res.status(400).send(errorResponse(resp400));
        return;
    }

    // POWER_MAX_100
    if( fighterToInsert.power === 0 || 
        (fighterToInsert.power && 
            (fighterToInsert.power > 100 || fighterToInsert.power < 0))) {
        // console.log('CREATE POWER_MAX_100', notValid);
        res.status(400).send(errorResponse(resp400));
        return;
    }

    // DEFENSE_CHECK
    if(typeof fighterToInsert.defense !== 'number' || fighterToInsert.defense < 1 || fighterToInsert.defense > 10) {
        res.status(400).send(errorResponse(resp400));
        return;
    }

    next();
}

const updateFighterValid = (req, res, next) => {
    const fighterToInsert = req.body;

    // EMPTY
    if (!fighterToInsert) {
        res.status(400).send(errorResponse(resp400));
        // console.log('UPDATE EMPTY');
        return;
    }
    
    // ID_SHOULD_BE_ABSENT
    if (fighterToInsert.id) {
        res.status(400).send(errorResponse(resp400));
        // console.log('UPDATE ID_SHOULD_BE_ABSENT');
        return;
    }

    notValid = Object.keys(fighterToInsert).find(key => {
        return fighter[key] === undefined && key !== 'id' && key !== 'health';
    });

    // ALL_FIELDS_MATCH    
    if (notValid) {
        res.status(400).send(errorResponse(resp400));
        // console.log('UPDATE ALL_FIELDS_MATCH');
        return;
    }

    // POWER_MAX_100
    if( fighterToInsert.power === 0 || 
        (fighterToInsert.power && 
            (fighterToInsert.power > 100 || fighterToInsert.power < 0))) {
        // console.log('CREATE POWER_MAX_100', notValid);
        res.status(400).send(errorResponse(resp400));
        return;
    }

    // DEFENSE_CHECK
    if(fighterToInsert.defense !== undefined && (typeof fighterToInsert.defense !== 'number' || fighterToInsert.defense < 1 || fighterToInsert.defense > 10)) {
        res.status(400).send(errorResponse(resp400));
        return;
    }

    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;