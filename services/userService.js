const { UserRepository } = require('../repositories/userRepository');

class UserService {
    getAll() {
        const data = UserRepository.getAll();
        return data && data.length ? data : null;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    getById(id) {
        const item = UserRepository.getOne({ id });
        if(!item) {
            return null;
        }
        return item;
    }

    create(user) {
        user.email = user.email.toLowerCase();
        const userByEmail = UserRepository.getOne({email: user.email});
        const userByPhone = UserRepository.getOne({phoneNumber: user.phoneNumber});
        if (userByEmail || userByPhone) {
            throw Error('User already exists');
        }
        return UserRepository.create(user);
    }

    update(id, dataToUpdate) {
        const user = UserRepository.getOne({ id });
        if(!user) {
            return null;
        }
        return UserRepository.update(id, dataToUpdate);
    }

    delete(id) {
        const deleteResult = UserRepository.delete(id);
        if(deleteResult && deleteResult.length) {
            return deleteResult;
        }
        return null;
    }
}

module.exports = new UserService();