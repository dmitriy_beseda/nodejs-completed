const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
    getAll() {
        const data = FightRepository.getAll();
        return data && data.length ? data : null;
    }

    search(search) {
        const item = FightRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    getById(id) {
        const item = FightRepository.getOne({ id });
        if(!item) {
            return null;
        }
        return item;
    }

    create(fighter) {
        return FightRepository.create(fighter);
    }

    update(id, dataToUpdate) {
        const fighter = FightRepository.getOne({ id });
        if(!fighter) {
            return null;
        }
        return FightRepository.update(id, dataToUpdate);
    }

    delete(id) {
        const deleteResult = FightRepository.delete(id);
        if(deleteResult && deleteResult.length) {
            return deleteResult;
        }
        return null;
    }
}

module.exports = new FightersService();