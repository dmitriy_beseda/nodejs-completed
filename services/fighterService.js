const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    getAll() {
        const data = FighterRepository.getAll();
        return data && data.length ? data : null;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    getById(id) {
        const item = FighterRepository.getOne({ id });
        if(!item) {
            return null;
        }
        return item;
    }

    create(fighter) {
        const fighters = FighterRepository.getAll();
        if (fighters.some(it => it.name.toLowerCase() === fighter.name.toLowerCase())) {
            throw Error('Fighter already exists');
        }
        fighter.health = 100;
        return FighterRepository.create(fighter);
    }

    update(id, dataToUpdate) {
        const fighter = FighterRepository.getOne({ id });
        if(!fighter) {
            return null;
        }
        return FighterRepository.update(id, dataToUpdate);
    }

    delete(id) {
        const deleteResult = FighterRepository.delete(id);
        if(deleteResult && deleteResult.length) {
            return deleteResult;
        }
        return null;
    }
}

module.exports = new FighterService();