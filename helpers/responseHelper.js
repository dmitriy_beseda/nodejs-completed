exports.errorResponse = (message) => {
    return {
        error: true,
        message
    }
};

exports.resp400 = 'User Entity not valid';