const request = require('request');

const ping = (url) => new Promise((resolve, reject) => {
	return request(url, function(error, response) {
		if (error || response.statusCode !== 200) {
			reject(new Error('The URL ' + url + ' is not accessible. Make sure the repository is public.'));
		} else {
			resolve(true);
		}
	})
});

const makeRequest = (url, method = 'get', body = {}) => new Promise((resolve, reject) => {
    
	return request[method](url, {form: body}, (error, response) => {
		if (error) {
			reject(error);
		} else if (response.statusCode !== 200) {
			reject(new Error('The URL ' + url + ' is not accessible =(. Status code: ' + response.statusCode + '. Body: ' + response.body));
		} else {
			resolve(response.body);
		}
	});
});

module.exports = {
	ping,
	makeRequest,
};