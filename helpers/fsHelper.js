const fs = require('fs');
const rimraf = require('rimraf');
const _mkdirp = require('mkdirp');

const remkdir = (dirPath) => {
	if (fs.existsSync(dirPath)) {
		rimraf.sync(dirPath);
	}

	return _mkdirp(dirPath);
};

const mkdirp = (dirPath) => {
	if (fs.existsSync(dirPath)) {
		return;
	}

	return _mkdirp(dirPath);
};

const rmrf = (dirPath) => {
	rimraf.sync(dirPath);
};

module.exports = {
	remkdir,
	mkdirp,
	rmrf
};