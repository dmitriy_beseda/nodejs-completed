const fs = require('fs');
const path = require('path');
const fsHelper = require('./fsHelper');

const write = async (type, message) => {
	const dir = path.resolve(__dirname, '..', '.tmp');
	await fsHelper.mkdirp(dir);
	fs.writeFileSync(path.join(dir, 'log.txt'), `${(new Date()).toLocaleString('en-US')} ${type} ${message}\n`, {
		flag: 'a+'
	});
};

module.exports = {
	async info(message) {
		return await write('[INFO]', message);
	},

	async error(error) {
		return await write('[ERROR]', 'message: ' + error.message + '\n\n' + 'stack:' + error.stack);
	}
};