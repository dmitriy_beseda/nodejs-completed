const express = require('express')
const cors = require('cors');
const fs = require('fs');
const path = require('path');
const { exec } = require('child_process');
const rimraf = require("rimraf");
const axios = require('axios');
require('./config/db');

const app = express()

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const routes = require('./routes/index');
routes(app);

app.use('/', express.static('./client/build'));

app.post('/check', (req, res, next) => {
    const repoUrl = req.body.userRepository;
    const lang = 'ua';
    const token = req.body.token;
    const hometaskId = req.body.userHometaskId;

    const cmd = `. test.sh ${repoUrl} ${lang} ${token} ${hometaskId}`;

    console.log('SART CHECK');
    console.log(cmd);

    const resultTxt = `${path.resolve()}/result.txt`;
    const resultJson = `${path.resolve()}/result.json`;
    const tempDir = `${path.resolve()}/temp-repo`;
    if(fs.existsSync(resultTxt)) {
        fs.unlinkSync(resultTxt);
    }

    if(fs.existsSync(resultJson)) {
        fs.unlinkSync(resultJson);
    }

    if(fs.existsSync(tempDir)) {
        rimraf.sync(tempDir);
    }
    
    exec(cmd, (err, stdout, stderr) => {
        if (err) {
            // node couldn't execute the command
            console.log(`ERR: ${err}`);
            return;
        }

        console.log('DONE');

        const resultTempTxt = `${path.resolve()}/temp-repo/result.txt`;
        const resultTempJson = `${path.resolve()}/temp-repo/result.json`;

        const respUrl = endpoint;

        if(fs.existsSync(resultTempTxt)) {
            const file = fs.readFileSync(resultTempTxt);
            const body = {
                token,
                mark: 7,
                userHometaskId: hometaskId,
                generatedFeedback: file.toString(),
                trace: 'TRACE'
            };
    
            axios.post(respUrl, body, {});
            res.send(200, {message: 'success'});
            console.log('SUCCESS');
            return;
        } else {
            console.log('FILE NOT FOUND');
        }

        res.send(400);
        // next();
    });
});

const port = 3010;
app.listen(port, () => {});

exports.app = app;