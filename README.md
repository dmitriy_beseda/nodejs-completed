# Завдання

Написати серверну частину за допомогою NodeJS та Express для реалізації CRUD сутності користувача

* Використати стурктуру зі слоями с темплейту
* Зробити валідацію по моделі користувача через міддлвар

### Модель користувача:
```
user = {
    firstName: string,
    lastName: string,
    email: string,
    phoneNumber: string. regexp +380xxxxxxxxx
}
```
